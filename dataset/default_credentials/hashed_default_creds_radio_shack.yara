/*This Yara ruleset is under the GNU-GPLv2 license (http://www.gnu.org/licenses/gpl-2.0.html) and open to any user or organization, as long as you use it under this license.*/

rule nthash_hashed_default_creds_radio_shack
{
    meta:
        id = "2nqLYb6qAWvDsiJklO8UXj"
        fingerprint = "63fefa4f450b70eb512e6de244c35bb87eebb58a714ab4b8b2f8b64f29dfec59"
        version = "1.0"
        modified = "2024-02-15"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT INDER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft fürSicherheit in der Informationstechnik mbH & Co KG"
        description = "Hashed values of default credentials for radio_shack."
        category = "INFO"
        info = "NTHASH"
        reference = "https://gitlab.com/ndaal_open_source/ndaal_yara_passwords_default/-/tree/main/yara_rules"

strings:
    $a0="439482dc37b90b83bd7c1c33d5eb5bbf"
    $a1="85209dc141c8e381e708acd9fee861ac"
condition:
    ($a0 and $a1)
}

rule mysql323_hashed_default_creds_radio_shack
{
    meta:
        version = "1.0"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT INDER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft fürSicherheit in der Informationstechnik mbH & Co KG"
        description = "Hashed values of default credentials for radio_shack."
        category = "INFO"
        info = "MYSQL323"
        reference = "https://gitlab.com/ndaal_open_source/ndaal_yara_passwords_default/-/tree/main/yara_rules"

strings:
    $a0="76860dd10d2786f0"
    $a1="2a54f6c260937508"
condition:
    ($a0 and $a1)
}

rule mysql41_hashed_default_creds_radio_shack
{
    meta:
        version = "1.0"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT INDER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft fürSicherheit in der Informationstechnik mbH & Co KG"
        description = "Hashed values of default credentials for radio_shack."
        category = "INFO"
        info = "MYSQL41"
        reference = "https://gitlab.com/ndaal_open_source/ndaal_yara_passwords_default/-/tree/main/yara_rules"

strings:
    $a0="*78D6A9EA16AF19C85E9D2DCC9F457D6FE86E4F04"
    $a1="*62E9462699BB9AE33411B5A4B4E77A9E0F12CBBC"
condition:
    ($a0 and $a1)
}

rule ldap_md5_hashed_default_creds_radio_shack
{
    meta:
        version = "1.0"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT INDER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft fürSicherheit in der Informationstechnik mbH & Co KG"
        description = "Hashed values of default credentials for radio_shack."
        category = "INFO"
        info = "LDAP_MD5"
        reference = "https://gitlab.com/ndaal_open_source/ndaal_yara_passwords_default/-/tree/main/yara_rules"

strings:
    $a0="{MD5}BTf7QKaMGNpZo1wr/hylVA=="
    $a1="{MD5}WopHzbhEEHX84EwWHpGsHQ=="
condition:
    ($a0 and $a1)
}

rule ldap_sha1_hashed_default_creds_radio_shack
{
    meta:
        version = "1.0"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT INDER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft fürSicherheit in der Informationstechnik mbH & Co KG"
        description = "Hashed values of default credentials for radio_shack."
        category = "INFO"
        info = "LDAP_SHA1"
        reference = "https://gitlab.com/ndaal_open_source/ndaal_yara_passwords_default/-/tree/main/yara_rules"

strings:
    $a0="{SHA}GTs0N6lDdFeXcvPxqMjwg4Ehi/k="
    $a1="{SHA}YX3RgjqcoI7/I6k1wnLaEMmItfQ="
condition:
    ($a0 and $a1)
}

rule md5_hashed_default_creds_radio_shack
{
    meta:
        version = "1.0"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT INDER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft fürSicherheit in der Informationstechnik mbH & Co KG"
        description = "Hashed values of default credentials for radio_shack."
        category = "INFO"
        info = "MD5"
        reference = "https://gitlab.com/ndaal_open_source/ndaal_yara_passwords_default/-/tree/main/yara_rules"

strings:
    $a0="0537fb40a68c18da59a35c2bfe1ca554"
    $a1="5a8a47cdb8441075fce04c161e91ac1d"
condition:
    ($a0 and $a1)
}

rule sha1_hashed_default_creds_radio_shack
{
    meta:
        version = "1.0"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT INDER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft fürSicherheit in der Informationstechnik mbH & Co KG"
        description = "Hashed values of default credentials for radio_shack."
        category = "INFO"
        info = "SHA1"
        reference = "https://gitlab.com/ndaal_open_source/ndaal_yara_passwords_default/-/tree/main/yara_rules"

strings:
    $a0="193b3437a94374579772f3f1a8c8f08381218bf9"
    $a1="617dd1823a9ca08eff23a935c272da10c988b5f4"
condition:
    ($a0 and $a1)
}

rule sha384_hashed_default_creds_radio_shack
{
    meta:
        version = "1.0"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT INDER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft fürSicherheit in der Informationstechnik mbH & Co KG"
        description = "Hashed values of default credentials for radio_shack."
        category = "INFO"
        info = "SHA384"
        reference = "https://gitlab.com/ndaal_open_source/ndaal_yara_passwords_default/-/tree/main/yara_rules"

strings:
    $a0="2d6b69ebd8df1518c295aadb6c5eaff0b120b4d9d1eeb435cb9b4df80a3d2b4b22a4b820780f51271136e5b6790e7f52"
    $a1="99d9d91ba267eda91e4cd400fe2b30a405da8e7d9f9a260fe2e128ae2536bed5258b933a35d0c29dd2278fe56da112d4"
condition:
    ($a0 and $a1)
}

rule sha224_hashed_default_creds_radio_shack
{
    meta:
        version = "1.0"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT INDER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft fürSicherheit in der Informationstechnik mbH & Co KG"
        description = "Hashed values of default credentials for radio_shack."
        category = "INFO"
        info = "SHA224"
        reference = "https://gitlab.com/ndaal_open_source/ndaal_yara_passwords_default/-/tree/main/yara_rules"

strings:
    $a0="e19deae1228dde507418d1f8e7b68f7763bcbf7ffdc52b55791c7dce"
    $a1="f1b54a4843d22631aba8fa60caf2e8dcb6a3ef9f613439db0b9ab6b2"
condition:
    ($a0 and $a1)
}

rule sha512_hashed_default_creds_radio_shack
{
    meta:
        version = "1.0"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT INDER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft fürSicherheit in der Informationstechnik mbH & Co KG"
        description = "Hashed values of default credentials for radio_shack."
        category = "INFO"
        info = "SHA512"
        reference = "https://gitlab.com/ndaal_open_source/ndaal_yara_passwords_default/-/tree/main/yara_rules"

strings:
    $a0="fe3b6358428a7d041bbf76e6088ca90133ba9cd34414e3ed8c53eff2bcb2a940164ac3541d4a71a2d566b68f01874aa53440d1a87e2d0433f8b07a898dcccfe7"
    $a1="515d4e81bc825cefa65f8a9cdef035f50b688f7169a2e6cdfbe0e6143e5f5f31919de61dfd323876f49987cb5d9f9f0935d0d70884096a80283776910e76540c"
condition:
    ($a0 and $a1)
}

rule sha256_hashed_default_creds_radio_shack
{
    meta:
        version = "1.0"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT INDER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft fürSicherheit in der Informationstechnik mbH & Co KG"
        description = "Hashed values of default credentials for radio_shack."
        category = "INFO"
        info = "SHA256"
        reference = "https://gitlab.com/ndaal_open_source/ndaal_yara_passwords_default/-/tree/main/yara_rules"

strings:
    $a0="a15faf6f6c7e4c11d7956175f4a1c01edffff6e114684eee28c255a86a8888f8"
    $a1="43f98918052bdb9677a20185bfdc04f5bc3d2d9f2c686f1bdfbc399b5a4fda89"
condition:
    ($a0 and $a1)
}

rule blake2b_hashed_default_creds_radio_shack
{
    meta:
        version = "1.0"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT INDER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft fürSicherheit in der Informationstechnik mbH & Co KG"
        description = "Hashed values of default credentials for radio_shack."
        category = "INFO"
        info = "BLAKE2B"
        reference = "https://gitlab.com/ndaal_open_source/ndaal_yara_passwords_default/-/tree/main/yara_rules"

strings:
    $a0="9849fb3db2c78c49af0d2eeee88c7f0a203e523fb303f47f531486c4a2bb658bdd8aa59cff04c9a18a5fe1f2b2b3c674607d3020868549e8435676add59298d3"
    $a1="892c51ab4cd4321cedf7488172499d158b1a43ba46e138e28c8f03cbc5029ec88069ee1b5084eaf7df7521068223d7ae8833993d3bfc52dd229ab1bb45029391"
condition:
    ($a0 and $a1)
}

rule blake2s_hashed_default_creds_radio_shack
{
    meta:
        version = "1.0"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT INDER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft fürSicherheit in der Informationstechnik mbH & Co KG"
        description = "Hashed values of default credentials for radio_shack."
        category = "INFO"
        info = "BLAKE2S"
        reference = "https://gitlab.com/ndaal_open_source/ndaal_yara_passwords_default/-/tree/main/yara_rules"

strings:
    $a0="698e720f8746d03856ba1a51bcf5f576051e27a5dcb1008591086a30db1259f2"
    $a1="f169f69436b4384a360674f38ae8156a43ed0dda9807015cb0aac836c7ab5051"
condition:
    ($a0 and $a1)
}

rule sha3_224_hashed_default_creds_radio_shack
{
    meta:
        version = "1.0"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT INDER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft fürSicherheit in der Informationstechnik mbH & Co KG"
        description = "Hashed values of default credentials for radio_shack."
        category = "INFO"
        info = "SHA3_224"
        reference = "https://gitlab.com/ndaal_open_source/ndaal_yara_passwords_default/-/tree/main/yara_rules"

strings:
    $a0="6e87e82a698325c15d855224bf0a9df5dad636ecfb513e73421129ab"
    $a1="6308a4c5216491783a5382b1e26c795b0e2a42374b292eff64447c5c"
condition:
    ($a0 and $a1)
}

rule sha3_256_hashed_default_creds_radio_shack
{
    meta:
        version = "1.0"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT INDER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft fürSicherheit in der Informationstechnik mbH & Co KG"
        description = "Hashed values of default credentials for radio_shack."
        category = "INFO"
        info = "SHA3_256"
        reference = "https://gitlab.com/ndaal_open_source/ndaal_yara_passwords_default/-/tree/main/yara_rules"

strings:
    $a0="3efad2b03b031d2d7c8bb3b19f66eb66dfec0d70a09a8ead2f3a66bf77eac333"
    $a1="faef749b7f8d82a0c144e8f53fd815fc179f1cc40870f831046da2c89e2fc010"
condition:
    ($a0 and $a1)
}

rule sha3_384_hashed_default_creds_radio_shack
{
    meta:
        version = "1.0"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT INDER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft fürSicherheit in der Informationstechnik mbH & Co KG"
        description = "Hashed values of default credentials for radio_shack."
        category = "INFO"
        info = "SHA3_384"
        reference = "https://gitlab.com/ndaal_open_source/ndaal_yara_passwords_default/-/tree/main/yara_rules"

strings:
    $a0="cf9d0665d7e0828c6b69ebeb68dc546b21d2eac58ddba1e6798617b5abada050932c63e88c5e80023825fb9c05f4b034"
    $a1="512ab230fd8feab08a9ffe4521de2b0b28dd973768d7887ff2c421ced7cb1b309be5a01fee57f1f588f204e0b8e75f2d"
condition:
    ($a0 and $a1)
}

rule sha3_512_hashed_default_creds_radio_shack
{
    meta:
        version = "1.0"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT INDER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft fürSicherheit in der Informationstechnik mbH & Co KG"
        description = "Hashed values of default credentials for radio_shack."
        category = "INFO"
        info = "SHA3_512"
        reference = "https://gitlab.com/ndaal_open_source/ndaal_yara_passwords_default/-/tree/main/yara_rules"

strings:
    $a0="ad82bc8081f726e752f1478a301f4b8856660af1e97e67a069123070a88a770163134e8d2b53d74f2474c06687c2504181884ba307d41a753287945390509675"
    $a1="b9a260a5401fb3e4dcf171db05717188e2132dc786ff8bb20b2de7b6c6c4c00f870ddde06e8f6210b15ef2e615809bd0acc43cd71aefc4cc38043b06730d2ebd"
condition:
    ($a0 and $a1)
}

rule base64_hashed_default_creds_radio_shack
{
    meta:
        version = "1.0"
        status = "RELEASED"
        sharing = "TLP:WHITE"
        source = "NDAAL GESELLSCHAFT FÜR SICHERHEIT INDER INFORMATIONSTECHNIK MBH & CO KG"
        author = "Alaa Jubakhanji@ndaal Gesellschaft fürSicherheit in der Informationstechnik mbH & Co KG"
        description = "Hashed values of default credentials for radio_shack."
        category = "INFO"
        info = "SHA3_512"
        reference = "https://gitlab.com/ndaal_open_source/ndaal_yara_passwords_default/-/tree/main/yara_rules"

strings:
    $a0="W01VTFRJUExFXQ=="
    $a1="NzQ0"
condition:
    ($a0 and $a1)
}

